within SimDevTools.Functions;
function readRealVector "Read a Real[:] form an HDF5 file"
  input String fileName "File Name";
  input String datasetName "Dataset Name";
  input String quantity = "" "Expected Quantity (optional)";
  input String unit = "" "Expected Unit (optional)";
  output Real data[getDatasetDims(fileName, datasetName)*{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}];
  external "C" ModelicaHDF5Functions_read_dataset_double(fileName, datasetName, quantity, unit, data) annotation (
  Include="#include \"ModelicaHDF5Functions.h\"",
  Library={"ModelicaHDF5Functions", "libhdf5", "libhdf5_hl"},
  IncludeDirectory="modelica://SimDevTools/Resources/Include",
  LibraryDirectory="modelica://SimDevTools/Resources/Library");
end readRealVector;
