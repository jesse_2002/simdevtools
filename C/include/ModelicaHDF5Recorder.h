/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#ifndef MODELICAHDF5RECORDER_H_
#define MODELICAHDF5RECORDER_H_

#if defined(__cplusplus)
	#define EXTERN_API extern "C"
#else
	#define EXTERN_API extern
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ModelicaHDF5Recorder_recorder_t * ModelicaHDF5Recorder_recorder_h;

/*! Opens a recording session
 * 
 * @param [in]	filename			file name of the HDF5 result file
 * @param [in]	num_signals			number of signals
 * @param [in]	signal_names		signal names
 * @param [in]	signal_quantities	signal quantities
 * @param [in]	signal_units		signal units
 * @param [in]	signal_comments		signal comments
 *
 * @return		0 on success, -1 otherwise
 */
EXTERN_API ModelicaHDF5Recorder_recorder_h ModelicaHDF5Recorder_open(
	const char* filename, 
	int num_signals, 
	const char **signal_names, 
	const char **signal_quantities,
	const char **signal_units,
	const char **signal_display_units,
	const char **signal_comments, 
	const char *sdf_filename);

/*! Records one sample
 * 
 * @param [in]	time			timestamp of the sample
 * @param [in]	num_signals		number of signals
 * @param [in]	signal_values	signal values
 *
 * @return		0 on success, -1 otherwise
 */
EXTERN_API void ModelicaHDF5Recorder_record(ModelicaHDF5Recorder_recorder_h recorder, double time, int num_signals, const double *signal_values);

/*! Closes a recording session
 *
 * @return		0 on success, -1 otherwise
 */
EXTERN_API void ModelicaHDF5Recorder_close(ModelicaHDF5Recorder_recorder_h recorder);

#ifdef __cplusplus
}
#endif

#endif /*MODELICAHDF5RECORDER_H_*/