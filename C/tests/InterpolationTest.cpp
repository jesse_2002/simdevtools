/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#define _USE_MATH_DEFINES

#include <math.h>
#include <float.h>
#include "gtest/gtest.h"
#include "NDTable.h"


#ifdef _MSC_VER
static const unsigned long __nan[2] = { 0xffffffff, 0x7fffffff };
#define NAN (*(const float *) __nan)
#define INFINITY (DBL_MAX + DBL_MAX)
#endif

#define N_NON_FINITE 3
static const double non_finite[N_NON_FINITE] = { NAN, INFINITY, -INFINITY };  

TEST(InterpolationTest, FindIndex) {
	double value = 1.2;
	double values[4] = { 0, 1, 2, 3 };
	int num_values = 4;
	int index;
	double t;

	NDTable_find_index(value, num_values, values, &index, &t, NDTABLE_EXTRAP_HOLD);
}

TEST(InterpolationTest, interp_nearest) {
	double x[2] = { 0, 1 };
	double y[2] = { 2, 3 };

	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = 2;
	ds.numel = 2;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 0 };
	int nsubs[1];
	double t[1];

	double value;
	double derivatives[1];

	int i;

	// interpolate left boundary
	t[0] = 0.0;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_NONE, &value, derivatives));
	EXPECT_DOUBLE_EQ(2.0, value);
	EXPECT_DOUBLE_EQ(0.0, derivatives[0]);

	// interpolate before step
	t[0] = 0.49;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_NONE, &value, derivatives));
	EXPECT_DOUBLE_EQ(2.0, value);
	EXPECT_DOUBLE_EQ(0.0, derivatives[0]);

	// interpolate after step
	t[0] = 0.51;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_NONE, &value, derivatives));
	EXPECT_DOUBLE_EQ(3.0, value);
	EXPECT_DOUBLE_EQ(0.0, derivatives[0]);

	// interpolate right boundary
	t[0] = 1.0;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_NONE, &value, derivatives));
	EXPECT_DOUBLE_EQ(3.0, value);
	EXPECT_DOUBLE_EQ(0.0, derivatives[0]);

	// non-finite values
	for(i = 0; i < N_NON_FINITE; i++) {
		// left
		y[0] = non_finite[i]; 
		y[1] = 3;
		t[0] = 0.0;
		EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_LINEAR, &value, derivatives));
		EXPECT_EQ(1, _isnan(value));
		EXPECT_EQ(1, _isnan(derivatives[0]));

		// right boundary
		y[0] = 3;
		y[1] = non_finite[i]; 
		t[0] = 1.0;
		EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_LINEAR, &value, derivatives));
		EXPECT_EQ(1, _isnan(value));
		EXPECT_EQ(1, _isnan(derivatives[0]));
	}
}

TEST(InterpolationTest, NDTable_interp_linear) {
	double x[2] = { 0, 1 };
	double y[2] = { 2, 3 };

	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = 2;
	ds.numel = 2;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 0 };
	int nsubs[1];
	double t[1] = { 0.3 };

	double value;
	double derivatives[1];

	int i;

	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_NONE, &value, derivatives));
	EXPECT_DOUBLE_EQ(2.3, value);
	EXPECT_DOUBLE_EQ(1.0, derivatives[0]);

	// non-finite values
	for(i = 0; i < N_NON_FINITE; i++) {
		// left value non-finite
		y[0] = non_finite[i];
		y[1] = 2;
		EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_NONE, &value, derivatives));
		EXPECT_EQ(1, _isnan(value));
		EXPECT_EQ(1, _isnan(derivatives[0]));

		// right value non-finite
		y[0] = non_finite[i];
		y[1] = 3;
		EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_NONE, &value, derivatives));
		EXPECT_EQ(1, _isnan(value));
		EXPECT_EQ(1, _isnan(derivatives[0]));
	}
}

TEST(InterpolationTest, NDTable_interp_akima) {
	#define N 8

	int i, j, k;

	double x[N];
	double y[N];

	for (i = 0; i < N; i++) {
		x[i] = i * ((double)N) / ((double) N-1);
		y[i] = sin(x[i] * M_PI);
	}
	
	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = N;
	ds.numel = N;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 1 };
	int nsubs[1];
	double t[1] = { 0.99 };

	double value;
	double derivatives[1];

	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_AKIMA, NDTABLE_EXTRAP_NONE, &value, derivatives));

	// non-finite values
	subs[0] = 2;
	t[0] = 0.5;
	for(i = 0; i < N_NON_FINITE; i++) {
		for(j = 0; j < N; j++) {
			for (k = 0; k < N; k++) {
				y[i] = sin(x[i] * M_PI);
			}
			y[j] = non_finite[i];
			EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_AKIMA, NDTABLE_EXTRAP_NONE, &value, derivatives));
			if (!_isnan(value)) {
				i = i;
			}
			EXPECT_EQ(1, _isnan(value));
			EXPECT_EQ(1, _isnan(derivatives[0]));
		}
	}
}

TEST(InterpolationTest, NDTable_extrap_hold) {
	double x[2] = { 0, 1 };
	double y[2] = { 2, 3 };

	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = 2;
	ds.numel = 2;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 0 };
	int nsubs[1];
	double t[1];

	double value;
	double derivatives[1];

	int i;

	// extrapolate left
	t[0] = -0.1;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_HOLD, &value, derivatives));
	EXPECT_DOUBLE_EQ(2.0, value);
	EXPECT_DOUBLE_EQ(0.0, derivatives[0]);

	// extrapolate right
	t[0] = 1.1;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_HOLD, &value, derivatives));
	EXPECT_DOUBLE_EQ(3.0, value);
	EXPECT_DOUBLE_EQ(0.0, derivatives[0]);

	// non-finite values
	for(i = 0; i < N_NON_FINITE; i++) {
		y[0] = y[1] = non_finite[i];

		// left
		t[0] = -0.1;
		EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_HOLD, &value, derivatives));
		EXPECT_EQ(1, _isnan(value));
		EXPECT_EQ(1, _isnan(derivatives[0]));

		// right
		t[0] = 1.1;
		EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_HOLD, &value, derivatives));
		EXPECT_EQ(1, _isnan(value));
		EXPECT_EQ(1, _isnan(derivatives[0]));
	}
}

TEST(InterpolationTest, NDTable_extrap_linear) {
	double x[2] = { 0, 1 };
	double y[2] = { 2, 3 };

	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = 2;
	ds.numel = 2;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 0 };
	int nsubs[1];
	double t[1];

	double value;
	double derivatives[1];

	int i, j;

	// extrapolate left
	t[0] = -1.0;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_LINEAR, &value, derivatives));
	EXPECT_DOUBLE_EQ(1.0, value);
	EXPECT_DOUBLE_EQ(1.0, derivatives[0]);

	// extrapolate right
	t[0] = 2.0;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_LINEAR, &value, derivatives));
	EXPECT_DOUBLE_EQ(4.0, value);
	EXPECT_DOUBLE_EQ(1.0, derivatives[0]);

	// non-finite values
	for(i = 0; i < N_NON_FINITE; i++) {
		for(j = 0; j < 2; j++) {
			y[0] = 2; y[1] = 3;
			y[j] = non_finite[i];
			EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_LINEAR, &value, derivatives));
			EXPECT_EQ(1, _isnan(value));
			EXPECT_EQ(1, _isnan(derivatives[0]));
		}
	}
}

TEST(InterpolationTest, NDTable_extrap_none) {
	double x[2] = { 0, 1 };
	double y[2] = { 2, 3 };

	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = 2;
	ds.numel = 2;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 0 };
	int nsubs[1];
	double t[1];

	double value;
	double derivatives[MAX_NDIMS];
	
	// extrapolate left
	t[0] = -1;
	EXPECT_EQ(-1, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_NONE, &value, derivatives));

	// extrapolate right
	t[0] = 2.0;
	EXPECT_EQ(-1, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_NONE, &value, derivatives));
}

TEST(InterpolationTest, Interpolation2D) {
	double params[2] = { 0.0, 1.0 };
	double value;
	ModelicaNDTable_h ds = NDTable_read("peaks.sdf", "z", 2, NULL, NULL, NULL, NULL, true);

	EXPECT_TRUE(ds != NULL);

	EXPECT_EQ(0, NDTable_evaluate(ds, 2, params, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_HOLD, &value));	
	EXPECT_EQ(0, NDTable_evaluate(ds, 2, params, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_HOLD, &value));
	EXPECT_EQ(0, NDTable_evaluate(ds, 2, params, NDTABLE_INTERP_AKIMA, NDTABLE_EXTRAP_HOLD, &value));
}

TEST(InterpolationTest, Buggy_Akima) {
	// the values below are taken from the python implementation

	double x[6] = { 0.673469, 0.795918, 0.918367, 1.040816, 1.163265, 1.285714 };
	double y[6] = { 1.146825, 1.845808, 2.535502, 3.100704, 3.460279, 3.578774 };

	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = 6;
	ds.numel = 6;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 2 };
	int nsubs[1];
	double t[1] = { 0.5 };

	double value;
	double derivatives[MAX_NDIMS];

	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_AKIMA, NDTABLE_EXTRAP_NONE, &value, derivatives));

	EXPECT_NEAR(2.8417, value, 1e-4);
}

TEST(InterpolationTest, dummy_dimension_1d) {
	// this tests checks the direct return of the sample value
	// without interpolation for dimensions with extent < 2

	// create a 1-d dataset with length 1 
	double x[1] = { 0 };
	double y[1] = { 1.1 };

	ModelicaNDTable_t ds;
	ds.data = y;
	ds.scales[0] = x;
	ds.dims[0] = 1;
	ds.numel = 1;
	ds.ndims = 1;
	ds.offs[0] = 1;

	int subs[1] = { 0 };
	int nsubs[1] = { 0 }; // not used
	double t[1] = { 0.0 };

	double value;
	double derivative;

	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_NEAREST, NDTABLE_EXTRAP_NONE, &value, &derivative));
	EXPECT_DOUBLE_EQ(y[0], value);
	EXPECT_DOUBLE_EQ(0, derivative);
}

TEST(InterpolationTest, dummy_dimension_2d) {
	// this tests checks the direct return of the sample value
	// without interpolation for dimensions with extent < 2

	// create a 2-d dataset with size (2,1) 
	double x[2] = { 0, 1 };
	double y[1] = { 1 };
	double z[2] = { 1, 2 };

	ModelicaNDTable_t ds;
	ds.data = z;
	ds.scales[0] = x;
	ds.scales[1] = y;
	ds.dims[0] = 2;
	ds.dims[1] = 1;
	ds.numel = 2;
	ds.ndims = 2;
	ds.offs[0] = 1;
	ds.offs[1] = 2;

	int subs[2] = { 0, 0 };
	int nsubs[2];
	double t[2];

	double value;
	double der_values[2];

	t[0] = -0.1; 
	t[1] = -0.1;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_HOLD, &value, der_values));
	EXPECT_DOUBLE_EQ(1.0, value);
	EXPECT_DOUBLE_EQ(0, der_values[0]);
	EXPECT_DOUBLE_EQ(0, der_values[1]);

	t[0] = 0.5; 
	t[1] = 0.5;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_HOLD, &value, der_values));
	EXPECT_DOUBLE_EQ(1.5, value);
	EXPECT_DOUBLE_EQ(1, der_values[0]);
	EXPECT_DOUBLE_EQ(0, der_values[1]);

	t[0] = 1.1; 
	t[1] = 1.1;
	EXPECT_EQ(0, NDTable_evaluate_internal(&ds, t, subs, nsubs, 0, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_HOLD, &value, der_values));
	EXPECT_DOUBLE_EQ(2.0, value);
	EXPECT_DOUBLE_EQ(0, der_values[0]);
	EXPECT_DOUBLE_EQ(0, der_values[1]);
}

TEST(InterpolationTest, DummyDimension2D) {
	double params[2];
	double value;
	ModelicaNDTable_h ds = NDTable_read("dummy_dim.sdf", "z", 2, NULL, NULL, NULL, NULL, true);
	EXPECT_TRUE(ds != NULL);

	//EXPECT_EQ(0, NDTable_evaluate(ds_id, 2, params, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_HOLD, &value));

	params[0] = -0.51;
	params[1] = 0.0;
	EXPECT_EQ(0, NDTable_evaluate(ds, 2, params, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_HOLD, &value));
	EXPECT_DOUBLE_EQ(1.0, value);

	params[0] = -0.50;
	params[1] = 0.0;
	EXPECT_EQ(0, NDTable_evaluate(ds, 2, params, NDTABLE_INTERP_LINEAR, NDTABLE_EXTRAP_HOLD, &value));
	EXPECT_DOUBLE_EQ(1.0, value);
}
