/**
 * Copyright (C) 2014 Modelon GmbH. All rights reserved.
 *
 * This file is part of the Simulation Development Tools.
 *
 * This program and the accompanying materials are made
 * available under the terms of the BSD 3-Clause License
 * which accompanies this distribution, and is available at
 * http://simdevtools.org/LICENSE.txt
 *
 * Contributors:
 *   Torsten Sommer <torsten.sommer@modelon.com> - Initial API and implementation
 */

#include <limits>
#include "gtest/gtest.h"
#include "NDTable.h"

class ValidationTest : public ::testing::Test {

protected:
	ModelicaNDTable_h ds;
	double u[3];
	double i[3];
	char error_message[MAX_MESSAGE_LENGTH];

	virtual void SetUp() {

		ds = NDTable_alloc_table();

		u[0] = 10.0;
		u[1] = 10.1;
		u[2] = 10.2;

		i[0] = 0.1;
		i[1] = 0.11;
		i[2] = 0.9;

		ds->filename = "data.sdf";
		ds->datasetname = "/i";
		ds->ndims = 1;
		ds->dims[0] = 3;
		ds->numel = 3;
		ds->offs[0] = 1;
		ds->data = i;
		ds->scales[0] = u;
		ds->data_quantity = "Current";
		ds->data_unit = "A";
		ds->scale_quantities[0] = "Voltage";
		ds->scale_units[0] = "V";

		// zero the dataset
		memset(error_message, 0, MAX_MESSAGE_LENGTH);
	}

};


TEST_F(ValidationTest, FileName) {
	EXPECT_STREQ("data.sdf", ds->filename);
}

TEST_F(ValidationTest, Rank) {
	int result;
	
	ds->ndims = -1;
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The rank of '/i' in 'data.sdf' must be in the range [0;32] but was -1", error_message);

	ds->ndims = 33;
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The rank of '/i' in 'data.sdf' must be in the range [0;32] but was 33", error_message);
}

TEST_F(ValidationTest, ExtentOfDimensions) {
	int result;
	
	ds->dims[0] = 0;
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("Extent of dimension 0 of '/i' in 'data.sdf' must be >=0 but was 0", error_message);
}

TEST_F(ValidationTest, NumberOfElements) {
	int result;
	
	ds->numel = 4;
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The size of '/i' in 'data.sdf' does not match its extent", error_message);
}

TEST_F(ValidationTest, Offsets) {
	int result;
	
	ds->offs[0] = 0;
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The offset[0] of '/i' in 'data.sdf' must be 1 but was 0", error_message);
}

TEST_F(ValidationTest, ScaleNotSet) {
	int result;
	
	ds->scales[0] = NULL;
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("Scale for dimension 0 of '/i' in 'data.sdf' is not set", error_message);
}

TEST_F(ValidationTest, ScaleMonotonicity) {
	int result;
	
	ds->scales[0][2] = ds->scales[0][1];
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("Scale for dimension 0 of '/i' in 'data.sdf' is not strictly monotonic increasing at index 2", error_message);
}

TEST_F(ValidationTest, PositiveInfinity) {
	int result;
	
	ds->data[1] = std::numeric_limits<double>::infinity();
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The data value at index 1 of '/i' in 'data.sdf' is not finite", error_message);
}

TEST_F(ValidationTest, NotANumber) {
	int result;
	
	ds->data[2] = std::numeric_limits<double>::quiet_NaN();
	result = NDTable_validate_table(ds);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The data value at index 2 of '/i' in 'data.sdf' is not finite", error_message);
}

TEST_F(ValidationTest, WrongDataQuantity) {
	int result;
	
	result = NDTable_assert_quantities_and_units(ds, 1, "Capacity", NULL, NULL, NULL);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("Dataset '/i' in 'data.sdf' has the wrong quantity. Expected 'Capacity' but was 'Current'.", error_message);
}

TEST_F(ValidationTest, WrongDataUnit) {
	int result;
	
	result = NDTable_assert_quantities_and_units(ds, 1, NULL, "F", NULL, NULL);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("Dataset '/i' in 'data.sdf' has the wrong unit. Expected 'F' but was 'A'.", error_message);
}

TEST_F(ValidationTest, WrongScaleQuantity) {
	int result;
	char *scaleQuantities[] = { "Capacity" };
	
	result = NDTable_assert_quantities_and_units(ds, 1, NULL, NULL, (const char **)scaleQuantities, NULL);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The scale for dimension 0 of dataset '/i' in 'data.sdf' has the wrong quantity. Expected 'Capacity' but was 'Voltage'.", error_message);
}

TEST_F(ValidationTest, WrongScaleUnit) {
	int result;
	char *scaleUnits[] = { "F" };
	
	result = NDTable_assert_quantities_and_units(ds, 1, NULL, NULL, NULL, (const char **)scaleUnits);
	EXPECT_EQ(-1, result);
	NDTable_get_error_message(MAX_MESSAGE_LENGTH, error_message);
	EXPECT_STREQ("The scale for dimension 0 of dataset '/i' in 'data.sdf' has the wrong unit. Expected 'F' but was 'V'.", error_message);
}